angular.module('giffindor')
  .factory('$localstorage', ['$window', function($window) {
    return {
      set: function(key, value) {
        $window.localStorage[key] = value;
      },
      get: function(key, defaultValue) {
        return $window.localStorage[key] || defaultValue;
      },
      setObject: function(key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function(key) {
        return JSON.parse($window.localStorage[key] || '{}');
      }
    }
}])
.service('GifRequester', function($http){
	this.randomGif = function () {
     return $http.get("http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC");
	};

  this.randomGifByTag = function (tag) {
    return $http.get("http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=" + tag);
  };

});
