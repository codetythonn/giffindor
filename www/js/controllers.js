angular.module('giffindor')

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
})

.controller('CategoriesCtrl', function($scope) {
  var defaultCategories = [
                            { name: 'Cats' },
                            { name: 'WTF' },
                            { name: 'Mindfuck' },
                            { name: 'Puppies' },
                            { name: 'Funny' }
                          ];
  $scope.categories = defaultCategories;
})

.controller('CategoryCtrl', function($scope, $stateParams, GifRequester) {
  $scope.requestNewGif = function(){
    GifRequester.randomGifByTag($stateParams.name)
      .then(function(response){
        $scope.gif_url = response.data.data.image_url;
      });
  }
  $scope.requestNewGif();
})

.controller('RandomCtrl', function($scope, GifRequester){
  $scope.requestNewGif = function(){
    GifRequester.randomGif()
      .then(function(response){
        $scope.gif_url = response.data.data.image_url;
      });
  }
  $scope.requestNewGif();
})

.controller('TrendingCtrl', function($scope, $http){
  $scope.requestNewGif = function(){
    $http.get("http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC")
      .then(function(response){
        gifs = response.data.data;
        random_gif = gifs[Math.floor(Math.random()*gifs.length)];
        $scope.gif_url = random_gif.images.fixed_height.url;
      });
  }
  $scope.requestNewGif();
});
